// 3. Get all items containing Vitamin A.

function vitaminAItems(data){
    return data.filter(items => items.contains.includes("Vitamin A"))
}
module.exports={vitaminAItems}