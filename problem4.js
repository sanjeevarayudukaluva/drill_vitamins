/*4. Group items based on the Vitamins that they contain in the following format:
{
    "Vitamin C": ["Orange", "Mango"],
    "Vitamin K": ["Mango"],
}

and so on for all items and all Vitamins.*/

function groupingItems(data) {
  return data.reduce((acc, items) => {
    if (!acc[items.contains]) {
      acc[items.contains] = [];
    }
    acc[items.contains].push(items.name);

    return acc;
  }, []);
}
module.exports = { groupingItems };
