// 5. Sort items based on number of Vitamins they contain.

function sortItemsBasedOnVitamins(data) {
  return data.sort((a, b) => b.contains.length -a.contains.length);
}

module.exports = { sortItemsBasedOnVitamins };
