//1. Get all items that are available

const { items } = require("./dataSet.cjs");


function availableItems(data) {
 return  data.filter(items=>items.available === true)
}
module.exports = { availableItems };
