//2. Get all items containing only Vitamin C.

const { items } = require("./dataSet.cjs");

function vitaminC(data) {
  return data.filter((items) => items.contains === "Vitamin C");
}
module.exports = { vitaminC };
